$(document).ready(function(){

		var windowWidth = $( window ).width();

		resizeLogo();
		bannerFix();
		
		$(document).scroll(function() {
			resizeLogo();
			bannerFix();
		});	


		function resizeLogo() {
		    var offset = parseInt($(".banner.banner-content").height());
		    var duration = 100;
		    
	    	// console.log($(window).scrollTop());
	        if ($(this).scrollTop() > 30) {
	            $(".header__logo").addClass("smaller");
	        } else {
	            if ($(".header__logo").hasClass("smaller")) {
	                $(".header__logo").removeClass("smaller");
	            }
	        }
		};

		function bannerFix() {
			var scrolled = $(document).scrollTop();
			var windowWidth = $( window ).width();

			console.log(windowWidth);



			if (windowWidth <= 414) {
				if (scrolled >= 60) {
					$('.banner.banner-content').addClass('fixed');
				} else {
					$('.banner.banner-content').removeClass('fixed');
				}

			} else if (windowWidth <= 768) {

				if (scrolled >= 70) {
					$('.banner.banner-content').addClass('fixed');
				} else {
					$('.banner.banner-content').removeClass('fixed');
				}

			} else {
				if (scrolled >= 80) {
					$('.banner.banner-content').addClass('fixed');
				} else {
					$('.banner.banner-content').removeClass('fixed');
				}
			}


			
		}
		


		$('.burger__icon').click( function(){
			$('.js-hamburger').toggleClass('is-active');
			if (windowWidth <= 575) {
				$('.banner.banner-content.fixed').toggleClass('hide');
			}
			$('.menu').slideToggle();
		});
	
});

