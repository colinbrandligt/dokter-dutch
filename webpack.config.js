var glob = require('glob');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

const extractCSS = new ExtractTextPlugin('bundle.min.css');

module.exports = [{
  mode: 'development',
  entry: {
    'bundle.min.css': [
		 path.resolve(__dirname, 'components/css/index.scss')
    ]
  },
        output: {
          path:  path.resolve(__dirname, 'dist'),
          filename: "[name]",
        },
        module: {
			rules: [
               { // sass / scss loader for webpack
	                test: /\.(sass|scss)$/,
	                loader: extractCSS.extract(['css-loader','sass-loader']),
               },
	         ]
        },
          plugins: [
            extractCSS,
          ],
        watch: true
}];