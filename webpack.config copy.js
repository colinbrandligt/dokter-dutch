var glob = require('glob');
var path = require('path');
const lodash = require('lodash');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

const extractCSS = new ExtractTextPlugin('bundle.min.css');
const extractJS = new ExtractTextPlugin('bundle.min.js');

module.exports = [{
  mode: 'production',
  entry: {
    'bundle.min.css': [
         path.resolve(__dirname, 'components/css/index.scss')
    ],
    'bundle.min.js': [
         // path.resolve(__dirname, 'components/js/jquery.min.js'),
         // path.resolve(__dirname, 'components/js/bootstrap.min.js,'),
         // path.resolve(__dirname, 'components/js/jquery.easing.min.js'),
         // path.resolve(__dirname, 'components/js/slick.min.js'),
         path.resolve(__dirname, 'components/js/index.js')
    ],

  },
        output: {
          path:  path.resolve(__dirname, 'dist'),
          filename: "[name]",
        },
        module: {
            rules: [
               { // sass / scss loader for webpack
                    test: /\.(sass|scss)$/,
                    loader: extractCSS.extract(['css-loader','sass-loader']),
               },
               {
                    test: /\.js$/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            plugins: ['lodash']
                        }
                    }
                }

             ]
        },
          plugins: [
            extractCSS,
            lodash
          ],
        watch: true
}];